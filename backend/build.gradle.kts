import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	val kotlinVersion = "2.0.10"

	id("org.springframework.boot") version "3.3.2"
	id("io.spring.dependency-management") version "1.1.6"
	kotlin("jvm") version kotlinVersion
	kotlin("plugin.spring") version kotlinVersion
	jacoco
	id("org.cyclonedx.bom") version "1.9.0"
}

group = "de.dnshome.ak131554"
version = "latest"

java {
	sourceCompatibility = JavaVersion.VERSION_21
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.2.0")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")

	// CVE-2022-1471
	implementation("org.yaml:snakeyaml:2.2")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("com.ninja-squad:springmockk:4.0.2")
}

tasks.withType<KotlinCompile> {
	dependsOn(tasks.cyclonedxBom)
	compilerOptions {
		freeCompilerArgs.add("-Xjsr305=strict")
		jvmTarget.set(JvmTarget.JVM_21)
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
	finalizedBy(tasks.jacocoTestReport)
}

jacoco {
	toolVersion = "0.8.11"
	reportsDirectory.set(layout.buildDirectory.dir("jacoco"))
}

tasks.jacocoTestReport {
	dependsOn(tasks.test)
	reports {
		xml.required.set(true)
		xml.outputLocation.set(file(layout.buildDirectory.get().toString() + File.separator.toString() + "jacoco" + File.separator.toString() + "jacoco.xml"))
		csv.required.set(false)
		html.outputLocation.set(layout.buildDirectory.dir("jacocoHtml"))
	}
}

tasks.cyclonedxBom {
	setIncludeConfigs(listOf("compileClasspath", "runtimeClasspath"))
	setProjectType("application")
	setDestination(project.file("."))
	setSchemaVersion("1.4")
	setOutputFormat("json")
	setIncludeLicenseText(true)
	setIncludeBomSerialNumber(false)
}

tasks.processTestResources {
	dependsOn(tasks.cyclonedxBom)
}