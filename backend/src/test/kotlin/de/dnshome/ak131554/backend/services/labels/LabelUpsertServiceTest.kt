package de.dnshome.ak131554.backend.services.labels

import de.dnshome.ak131554.backend.models.Label
import de.dnshome.ak131554.backend.repositories.LabelRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class LabelUpsertServiceTest
{
    private lateinit var labelRepository: LabelRepository
    private lateinit var labelUpsertService: LabelUpsertService

    @BeforeEach
    fun setup()
    {
        labelRepository = mockk {
            val captor = slot<Label>()
            every { save(capture(captor)) } answers {
                captor.captured
            }
        }
        labelUpsertService = LabelUpsertService(labelRepository)
    }

    @Test
    fun shouldSaveLabel()
    {
        val label = Label("Label 1", setOf("test 1", "test 2"))

        labelUpsertService.upsert(label)

        verify(exactly = 1) { labelRepository.save(eq(label)) }
    }
}