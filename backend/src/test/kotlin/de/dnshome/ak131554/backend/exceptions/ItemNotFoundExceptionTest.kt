package de.dnshome.ak131554.backend.exceptions

import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.models.Store
import org.junit.jupiter.api.Test
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`

class ItemNotFoundExceptionTest
{
    @Test
    fun testIt()
    {
        assertThat(ItemNotFoundException(Product::class, 123L).message, `is`("Item Product with id <123> not found"))
        assertThat(ItemNotFoundException(Store::class, 1).message, `is`("Item Store with id <1> not found"))
    }
}