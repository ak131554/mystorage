package de.dnshome.ak131554.backend.services.stores

import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.repositories.StoreRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.Optional

class StoreFindServiceTest
{
    private val store1 = Store(1, "Store 1", "In Basement")
    private val store2 = Store(2, "Store 2", "In Attic")

    private lateinit var storeRepository: StoreRepository
    private lateinit var storeFindService: StoreFindService

    @BeforeEach
    fun setup()
    {
        val idCaptor = slot<Int>()
        storeRepository = mockk {
            every { findById(capture(idCaptor)) } answers {
                when (idCaptor.captured)
                {
                    1    -> Optional.of(store1)
                    2    -> Optional.of(store2)
                    else -> Optional.empty()
                }
            }
        }
        storeFindService = StoreFindService(storeRepository)
    }

    @Test
    fun shouldReturnStore()
    {
        assertThat(storeFindService.find(1), `is`(store1))
        assertThat(storeFindService.find(2), `is`(store2))

        verify(exactly = 2) { storeRepository.findById(any()) }
    }

    @Test
    fun shouldThrowItemNotFoundException()
    {
        val ex = assertThrows<ItemNotFoundException> { storeFindService.find(999) }
        assertThat(ex.message, `is`("Item Store with id <999> not found"))

        verify(exactly = 1) { storeRepository.findById(any()) }
    }
}