package de.dnshome.ak131554.backend.services.stocks

import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.models.Stock
import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.repositories.StockRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.Matchers.empty
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.util.UUID

class StockShowServiceTest
{
    private val product1 = Product(1L, "Testprodukt 1", setOf("test1", "test2"))
    private val product2 = Product(2L, "Testprodukt 2", setOf("test1", "test3"))

    private val store1 = Store(1, "Store 1", "In Basement")
    private val store2 = Store(2, "Store 2", "In Attic")

    private val stock1 = Stock(UUID.randomUUID(), product1, store1, 10, LocalDate.of(2024, 12, 24))
    private val stock2 = Stock(UUID.randomUUID(), product2, store1, 15, LocalDate.of(2024, 2, 29))
    private val stock3 = Stock(UUID.randomUUID(), product1, store2, 20, LocalDate.of(2025, 6, 22))
    private val stock4 = Stock(UUID.randomUUID(), product2, store2, 25, LocalDate.of(2023, 6, 1))

    private lateinit var stockRepository: StockRepository
    private lateinit var stockShowService: StockShowService

    @BeforeEach
    fun setup()
    {
        stockRepository = mockk {
            every { findAll() } returns(listOf(stock1, stock2, stock3, stock4))

            val captor = slot<Long>()
            every { findAllByProductGlobalTradeItemNumber(capture(captor)) } answers {
                when (captor.captured)
                {
                    1L -> listOf(stock1, stock3)
                    2L -> listOf(stock2, stock4)
                    else     -> listOf()
                }
            }
        }
        stockShowService = StockShowService(stockRepository)
    }

    @Test
    fun shouldListAllStocks()
    {
        assertThat(stockShowService.listStocks(null), containsInAnyOrder(stock1, stock2, stock3, stock4))
        verify(exactly = 1) { stockRepository.findAll() }
    }

    @Test
    fun shouldListStocksFilteredByProduct()
    {
        assertThat(stockShowService.listStocks(1L), containsInAnyOrder(stock1, stock3))
        assertThat(stockShowService.listStocks(2L), containsInAnyOrder(stock2, stock4))
        assertThat(stockShowService.listStocks(3L), empty())

        verify(exactly = 3) { stockRepository.findAllByProductGlobalTradeItemNumber(any()) }
    }
}
