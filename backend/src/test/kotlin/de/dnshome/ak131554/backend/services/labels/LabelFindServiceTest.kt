package de.dnshome.ak131554.backend.services.labels

import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Label
import de.dnshome.ak131554.backend.repositories.LabelRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.Optional

class LabelFindServiceTest
{
    private val label1 = Label("Label 1", setOf("test 1", "test 2"))
    private val label2 = Label("Label 2", setOf("test 2", "test 3"))

    private lateinit var labelRepository: LabelRepository
    private lateinit var labelFindService: LabelFindService

    @BeforeEach
    fun setup()
    {
        val nameCaptor = slot<String>()
        labelRepository = mockk {
            every { findByName(capture(nameCaptor)) } answers {
                when (nameCaptor.captured)
                {
                    "Label 1" -> Optional.of(label1)
                    "Label 2" -> Optional.of(label2)
                    else      -> Optional.empty()
                }
            }
        }
        labelFindService = LabelFindService(labelRepository)
    }

    @Test
    fun shouldReturnLabel()
    {
        assertThat(labelFindService.find("Label 1"), `is`(label1))
        assertThat(labelFindService.find("Label 2"), `is`(label2))

        verify(exactly = 2) { labelRepository.findByName(any()) }
    }

    @Test
    fun shouldThrowItemNotFoundException()
    {
        val ex = assertThrows<ItemNotFoundException> { labelFindService.find("Label 5") }
        assertThat(ex.message, `is`("Item Label with id <Label 5> not found"))

        verify(exactly = 1) { labelRepository.findByName(any()) }
    }
}