package de.dnshome.ak131554.backend.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.services.products.ProductFindService
import de.dnshome.ak131554.backend.services.products.ProductShowService
import de.dnshome.ak131554.backend.services.products.ProductUpsertService
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.hamcrest.Matchers.hasSize
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.isA
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(controllers = [ProductController::class])
class ProductControllerTest
{
    private val product1 = Product(1L, "Testprodukt 1", setOf("test1", "test2"))
    private val product2 = Product(2L, "Testprodukt 2", setOf("test1", "test3"))

    @Autowired
    @MockkBean
    private lateinit var productShowService: ProductShowService

    @Autowired
    @MockkBean
    private lateinit var productFindService: ProductFindService

    @Autowired
    @MockkBean
    private lateinit var productUpsertService: ProductUpsertService

    @Autowired
    private lateinit var mvc: MockMvc

    @Nested
    inner class ListProductTest
    {
        @BeforeEach
        fun setup()
        {
            val captor = mutableListOf<String?>()
            every { productShowService.listProducts(null) } returns(listOf(product1, product2))
            every { productShowService.listProducts(captureNullable(captor)) } answers {
                when (captor.last())
                {
                    null    -> listOf(product1, product2)
                    "test1" -> listOf(product1, product2)
                    "test2" -> listOf(product1)
                    "test3" -> listOf(product2)
                    else    -> listOf()
                }
            }
        }

        private fun MockMvc.perform(label: String? = null): ResultActions
        {
            val requestBuilder = get("/products")
            label?.apply { requestBuilder.queryParam("label", this) }
            return perform(requestBuilder)
        }

        private fun ResultActions.andExpectOkAndContainsInAnyOrder(vararg products: Product) =
                andExpect(status().isOk).andExpect(content()
                        .contentType("application/json")).andExpectItContainsInAnyOrder(*products)

        private fun ResultActions.andExpectItContainsInAnyOrder(vararg products: Product)
        {
            this.andExpect(jsonPath("$", hasSize<Any>(products.size)))
            products.forEach { product ->
                this.andExpect(jsonPath("$.[?(@.globalTradeItemNumber == ${product.globalTradeItemNumber} && @.name == \"${product.name}\" && @.labels.length() == ${product.labels.size}${
                    product.labels.mapIndexed { index, s -> " && @.labels[$index] == \"$s\"" }
                            .joinToString("")
                })]")
                        .exists())
            }
        }

        @Test
        fun shouldReturnAllProducts()
        {
            mvc.perform().andExpectOkAndContainsInAnyOrder(product1, product2)
            verify(exactly = 1) { productShowService.listProducts(null) }
        }

        @Test
        fun shouldReturnProductsFilteredByLabel()
        {
            mvc.perform("test1").andExpectOkAndContainsInAnyOrder(product1, product2)
            mvc.perform("test2").andExpectOkAndContainsInAnyOrder(product1)
            mvc.perform("test3").andExpectOkAndContainsInAnyOrder(product2)

            mvc.perform("invalid").andExpectOkAndContainsInAnyOrder()

            verify(exactly = 4) { productShowService.listProducts(any()) }
        }
    }

    @Nested
    inner class GetProductTest
    {
        @BeforeEach
        fun setup()
        {
            val captor = slot<Long>()
            every { productFindService.find(capture(captor)) } answers {
                when (captor.captured)
                {
                    1L   -> product1
                    2L   -> product2
                    else -> throw ItemNotFoundException(Product::class, captor.captured)
                }
            }
        }

        private fun MockMvc.perform(id: Long): ResultActions = perform(get("/products/$id"))

        private fun ResultActions.andExpectOkAndProduct(product: Product) =
                andExpect(status().isOk).andExpect(content().contentType("application/json")).andExpectItIs(product)

        private fun ResultActions.andExpectItIs(product: Product)
        {
            this.andExpect(jsonPath("$", isA<Any>(LinkedHashMap::class.java)))
                    .andExpect(jsonPath("$.globalTradeItemNumber", `is`(product.globalTradeItemNumber.toInt())))
                    .andExpect(jsonPath("$.name", `is`(product.name)))
            this.andExpect(jsonPath("$.labels").isArray)
                    .andExpect(jsonPath("$.labels", hasSize<Any>(product.labels.size)))
            product.labels.forEachIndexed { index, s ->
                this.andExpect(jsonPath("$.labels[$index]", `is`(s)))
            }
        }

        @Test
        fun shouldReturnOneProductWhenLookingForExistingId()
        {
            mvc.perform(1L).andExpectOkAndProduct(product1)
            mvc.perform(2L).andExpectOkAndProduct(product2)

            verify(exactly = 2) { productFindService.find(any()) }
        }

        @Test
        fun shouldReturnNotFoundWhenLookingForNonExistingId()
        {
            mvc.perform(5L)
                    .andExpect(status().isNotFound)
                    .andExpect(content().contentType("text/plain;charset=UTF-8"))
                    .andExpect(content().string("Item Product with id <5> not found"))

            verify(exactly = 1) { productFindService.find(any()) }
        }
    }

    @Nested
    inner class SaveProductTest
    {
        private val objectMapper = ObjectMapper()

        private fun Product.toJson() = objectMapper.writeValueAsString(this)
        private fun MockMvc.perform(product: Product): ResultActions =
                perform(post("/products").contentType("application/json").content(product.toJson()))

        @BeforeEach
        fun setup()
        {
            every { productUpsertService.upsert(any()) } returns(Unit)
        }

        @Test
        fun shouldReturnOkWhenSavingProduct()
        {
            val product = Product(3L, "Testprodukt 3", setOf("testX", "testY"))
            mvc.perform(product)
                    .andExpect(status().isOk)
                    .andExpect(content().contentType("text/plain;charset=UTF-8"))
                    .andExpect(content().string("OK"))
        }
    }
}