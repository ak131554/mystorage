package de.dnshome.ak131554.backend.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.ninjasquad.springmockk.MockkBean
import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.models.Stock
import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.services.stocks.StockShowService
import de.dnshome.ak131554.backend.services.stocks.StockUpsertService
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.hamcrest.Matchers.hasSize
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.isA
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.LocalDate
import java.util.UUID

@WebMvcTest(controllers = [StockController::class])
class StockControllerTest
{
    private val product1 = Product(1L, "Testprodukt 1", setOf("test1", "test2"))
    private val product2 = Product(2L, "Testprodukt 2", setOf("test1", "test3"))

    private val store1 = Store(1, "Store 1", "In Basement")
    private val store2 = Store(2, "Store 2", "In Attic")

    private val stock1 = Stock(UUID.randomUUID(), product1, store1, 10, LocalDate.of(2024, 12, 24))
    private val stock2 = Stock(UUID.randomUUID(), product2, store1, 15, LocalDate.of(2024, 2, 29))
    private val stock3 = Stock(UUID.randomUUID(), product1, store2, 20, LocalDate.of(2025, 6, 22))
    private val stock4 = Stock(UUID.randomUUID(), product2, store2, 25, LocalDate.of(2023, 6, 1))

    @Autowired
    @MockkBean
    private lateinit var stockShowService: StockShowService

    @Autowired
    @MockkBean
    private lateinit var stockUpsertService: StockUpsertService

    @Autowired
    private lateinit var mvc: MockMvc

    @Nested
    inner class ListStockTest
    {
        @BeforeEach
        fun setup()
        {
            val captor = mutableListOf<Long?>()
            every { stockShowService.listStocks(captureNullable(captor)) } answers {
                when (captor.last())
                {
                    null -> listOf(stock1, stock2, stock3, stock4)
                    1L   -> listOf(stock1, stock3)
                    2L   -> listOf(stock2, stock4)
                    else -> listOf()
                }
            }
        }

        private fun MockMvc.perform(gtin: Long? = null) =
                perform(get("/stocks").also { builder -> gtin?.let { builder.queryParam("gtin", it.toString()) } })

        private fun ResultActions.andExpectOkAndContainsInAnyOrder(vararg stocks: Stock) =
                andExpect(status().isOk).andExpect(content()
                        .contentType("application/json")).andExpectItContainsInAnyOrder(*stocks)

        private fun ResultActions.andExpectItContainsInAnyOrder(vararg stocks: Stock)
        {
            this.andExpect(jsonPath("$", hasSize<Any>(stocks.size)))
            stocks.forEach { stock ->
                this.andExpect(jsonPath("$.[?(@.id == \"${stock.id}\" && @.product.globalTradeItemNumber == ${stock.product.globalTradeItemNumber} && @.product.name == \"${stock.product.name}\" && @.product.labels.length() == ${stock.product.labels.size}${
                    stock.product.labels.mapIndexed { index, s -> " && @.product.labels[$index] == \"$s\"" }
                            .joinToString("")
                } && @.store.id == ${stock.store.id} && @.store.name == \"${stock.store.name}\" && @.store.description == \"${stock.store.description}\" && @.amount == ${stock.amount} && @.bestBeforeDate == \"${stock.bestBeforeDate}\")]")
                        .exists())
            }
        }

        @Test
        fun shouldReturnAllStocks()
        {
            mvc.perform().andExpectOkAndContainsInAnyOrder(stock1, stock2, stock3, stock4)
            verify(exactly = 1) { stockShowService.listStocks(null) }
        }

        @Test
        fun shouldReturnProductsFilteredByProduct()
        {
            mvc.perform(1L).andExpectOkAndContainsInAnyOrder(stock1, stock3)
            mvc.perform(2L).andExpectOkAndContainsInAnyOrder(stock2, stock4)

            mvc.perform(5L).andExpectOkAndContainsInAnyOrder()

            verify(exactly = 3) { stockShowService.listStocks(any()) }
        }
    }

    @Nested
    inner class SaveStockTest
    {
        private val objectMapper = ObjectMapper().also { it.registerModule(JavaTimeModule()) }

        private fun Stock.toJson() = objectMapper.writeValueAsString(this)
        private fun MockMvc.perform(stock: Stock): ResultActions =
                perform(post("/stocks").contentType("application/json").content(stock.toJson()))

        @BeforeEach
        fun setup()
        {
            every { stockUpsertService.upsert(any()) } returns (Unit)
        }

        @Test
        fun shouldReturnOkWhenSavingStock()
        {
            val stock = Stock(UUID.randomUUID(),
                    Product(3L, "Testprodukt 3", setOf()),
                    Store(3, "Store 3", "Cupboard under the stairs"),
                    4,
                    LocalDate.of(1337, 4, 19))
            mvc.perform(stock)
                    .andExpect(status().isOk)
                    .andExpect(content().contentType("text/plain;charset=UTF-8"))
                    .andExpect(content().string("OK"))
        }
    }
}