package de.dnshome.ak131554.backend.services.stocks

import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.models.Stock
import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.repositories.StockRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.util.UUID

class StockUpsertServiceTest
{
    private lateinit var stockRepository: StockRepository
    private lateinit var stockUpsertService: StockUpsertService

    @BeforeEach
    fun setup()
    {
        stockRepository = mockk {
            val captor = slot<Stock>()
            every { save(capture(captor)) } answers {
                captor.captured
            }
        }
        stockUpsertService = StockUpsertService(stockRepository)
    }

    @Test
    fun shouldSaveStock()
    {
        val stock = Stock(UUID.randomUUID(),
                Product(1L, "Testprodukt 1", setOf()),
                Store(1, "Store 1", "Basement"),
                10,
                LocalDate.of(2024, 12, 24))

        stockUpsertService.upsert(stock)

        verify(exactly = 1) { stockRepository.save(eq(stock)) }
    }
}