package de.dnshome.ak131554.backend.services.products

import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.repositories.ProductRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.Matchers.empty

class ProductShowServiceTest
{
    private val product1 = Product(1L, "Testprodukt 1", setOf("test1", "test2"))
    private val product2 = Product(2L, "Testprodukt 2", setOf("test1", "test3"))

    private lateinit var productRepository: ProductRepository
    private lateinit var productShowService: ProductShowService

    @BeforeEach
    fun setup()
    {
        val labelCaptor = slot<String>()
        productRepository = mockk {
            every { findAll() } returns(listOf(product1, product2))
            every { findByLabelsContains(capture(labelCaptor)) } answers {
                when (labelCaptor.captured)
                {
                    "test1" -> listOf(product1, product2)
                    "test2" -> listOf(product1)
                    "test3" -> listOf(product2)
                    else    -> listOf()
                }
            }
        }
        productShowService = ProductShowService(productRepository)
    }

    @Test
    fun shouldListAllProducts()
    {
        assertThat(productShowService.listProducts(null), containsInAnyOrder(product1, product2))
        verify(exactly = 1) { productRepository.findAll() }
    }

    @Test
    fun shouldListProductsFilteredByLabel()
    {
        assertThat(productShowService.listProducts("test1"), containsInAnyOrder(product1, product2))
        assertThat(productShowService.listProducts("test2"), containsInAnyOrder(product1))
        assertThat(productShowService.listProducts("test3"), containsInAnyOrder(product2))

        assertThat(productShowService.listProducts("invalid"), empty())

        verify(exactly = 4) { productRepository.findByLabelsContains(any()) }
    }
}