package de.dnshome.ak131554.backend.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Label
import de.dnshome.ak131554.backend.services.labels.LabelFindService
import de.dnshome.ak131554.backend.services.labels.LabelShowService
import de.dnshome.ak131554.backend.services.labels.LabelUpsertService
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.hamcrest.Matchers.hasSize
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.isA
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(controllers = [LabelController::class])
class LabelControllerTest
{
    private val label1 = Label("Label 1", setOf("test1", "test2"))
    private val label2 = Label("Label 2", setOf("test1", "test3"))

    @Autowired
    @MockkBean
    private lateinit var labelShowService: LabelShowService

    @Autowired
    @MockkBean
    private lateinit var labelFindService: LabelFindService

    @Autowired
    @MockkBean
    private lateinit var labelUpsertService: LabelUpsertService

    @Autowired
    private lateinit var mvc: MockMvc

    @Nested
    inner class ListLabelTest
    {
        @BeforeEach
        fun setup()
        {
            every { labelShowService.listLabels() } returns(listOf(label1, label2))
        }

        private fun MockMvc.perform() = perform(get("/labels"))

        private fun ResultActions.andExpectOkAndContainsInAnyOrder(vararg labels: Label) =
                andExpect(status().isOk).andExpect(content()
                        .contentType("application/json")).andExpectItContainsInAnyOrder(*labels)

        private fun ResultActions.andExpectItContainsInAnyOrder(vararg labels: Label)
        {
            this.andExpect(jsonPath("$", hasSize<Any>(labels.size)))
            labels.forEach { label ->
                this.andExpect(jsonPath("$.[?(@.name == \"${label.name}\" && @.labels.length() == ${label.labels.size}${
                    label.labels.mapIndexed { index, s -> " && @.labels[$index] == \"$s\"" }
                            .joinToString("")
                })]")
                        .exists())
            }
        }

        @Test
        fun shouldReturnAllLabels()
        {
            mvc.perform().andExpectOkAndContainsInAnyOrder(label1, label2)
            verify(exactly = 1) { labelShowService.listLabels() }
        }
    }

    @Nested
    inner class GetLabelTest
    {
        @BeforeEach
        fun setup()
        {
            val captor = slot<String>()
            every { labelFindService.find(capture(captor)) } answers {
                when (captor.captured)
                {
                    "Label 1" -> label1
                    "Label 2" -> label2
                    else      -> throw ItemNotFoundException(Label::class, captor.captured)
                }
            }
        }

        private fun MockMvc.perform(name: String): ResultActions = perform(get("/labels/$name"))

        private fun ResultActions.andExpectOkAndLabel(label: Label) =
                andExpect(status().isOk).andExpect(content().contentType("application/json")).andExpectItIs(label)

        private fun ResultActions.andExpectItIs(label: Label)
        {
            this.andExpect(jsonPath("$", isA<Any>(LinkedHashMap::class.java)))
                    .andExpect(jsonPath("$.name", `is`(label.name)))
            this.andExpect(jsonPath("$.labels").isArray)
                    .andExpect(jsonPath("$.labels", hasSize<Any>(label.labels.size)))
            label.labels.forEachIndexed { index, s ->
                this.andExpect(jsonPath("$.labels[$index]", `is`(s)))
            }
        }

        @Test
        fun shouldReturnOneLabelWhenLookingForExistingId()
        {
            mvc.perform("Label 1").andExpectOkAndLabel(label1)
            mvc.perform("Label 2").andExpectOkAndLabel(label2)

            verify(exactly = 2) { labelFindService.find(any()) }
        }

        @Test
        fun shouldReturnNotFoundWhenLookingForNonExistingId()
        {
            mvc.perform("Label 5")
                    .andExpect(status().isNotFound)
                    .andExpect(content().contentType("text/plain;charset=UTF-8"))
                    .andExpect(content().string("Item Label with id <Label 5> not found"))

            verify(exactly = 1) { labelFindService.find(any()) }
        }
    }

    @Nested
    inner class SaveLabelTest
    {
        private val objectMapper = ObjectMapper()

        private fun Label.toJson() = objectMapper.writeValueAsString(this)
        private fun MockMvc.perform(label: Label): ResultActions =
                perform(post("/labels").contentType("application/json").content(label.toJson()))

        @BeforeEach
        fun setup()
        {
            every { labelUpsertService.upsert(any()) } returns(Unit)
        }

        @Test
        fun shouldReturnOkWhenSavingLabel()
        {
            val label = Label("Label 3", setOf("testX", "testY"))
            mvc.perform(label)
                    .andExpect(status().isOk)
                    .andExpect(content().contentType("text/plain;charset=UTF-8"))
                    .andExpect(content().string("OK"))
        }
    }
}