package de.dnshome.ak131554.backend.services.products

import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.repositories.ProductRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.Optional
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.assertThrows

class ProductFindServiceTest
{
    private val product1 = Product(1L, "Testprodukt 1", setOf("test1", "test2"))
    private val product2 = Product(2L, "Testprodukt 2", setOf("test1", "test3"))

    private lateinit var productRepository: ProductRepository
    private lateinit var productFindService: ProductFindService

    @BeforeEach
    fun setup()
    {
        val gtinCaptor = slot<Long>()
        productRepository = mockk {
            every { findById(capture(gtinCaptor)) } answers {
                when (gtinCaptor.captured)
                {
                    1L   -> Optional.of(product1)
                    2L   -> Optional.of(product2)
                    else -> Optional.empty()
                }
            }
        }
        productFindService = ProductFindService(productRepository)
    }

    @Test
    fun shouldReturnProduct()
    {
        assertThat(productFindService.find(1L), `is`(product1))
        assertThat(productFindService.find(2L), `is`(product2))

        verify(exactly = 2) { productRepository.findById(any()) }
    }

    @Test
    fun shouldThrowItemNotFoundException()
    {
        val ex = assertThrows<ItemNotFoundException> { productFindService.find(999L) }
        assertThat(ex.message, `is`("Item Product with id <999> not found"))

        verify(exactly = 1) { productRepository.findById(any()) }
    }
}