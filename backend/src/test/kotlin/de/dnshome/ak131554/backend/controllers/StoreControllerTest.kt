package de.dnshome.ak131554.backend.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.services.stores.StoreFindService
import de.dnshome.ak131554.backend.services.stores.StoreShowService
import de.dnshome.ak131554.backend.services.stores.StoreUpsertService
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.hamcrest.Matchers.hasSize
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.isA
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(controllers = [StoreController::class])
class StoreControllerTest
{
    private val store1 = Store(1, "Store 1", "In Basement")
    private val store2 = Store(2, "Store 2", "In Attic")

    @Autowired
    @MockkBean
    private lateinit var storeShowService: StoreShowService

    @Autowired
    @MockkBean
    private lateinit var storeFindService: StoreFindService

    @Autowired
    @MockkBean
    private lateinit var storeUpsertService: StoreUpsertService

    @Autowired
    private lateinit var mvc: MockMvc

    @Nested
    inner class ListStoreTest
    {
        @BeforeEach
        fun setup()
        {
            every { storeShowService.listStores() } returns(listOf(store1, store2))
        }

        private fun MockMvc.perform() = perform(get("/stores"))

        private fun ResultActions.andExpectOkAndContainsInAnyOrder(vararg stores: Store) =
                andExpect(status().isOk).andExpect(content()
                        .contentType("application/json")).andExpectItContainsInAnyOrder(*stores)

        private fun ResultActions.andExpectItContainsInAnyOrder(vararg stores: Store)
        {
            this.andExpect(jsonPath("$", hasSize<Any>(stores.size)))
            stores.forEach { store ->
                this.andExpect(jsonPath("$.[?(@.id == ${store.id} && @.name == \"${store.name}\" && @.description == \"${store.description}\")]")
                        .exists())
            }
        }

        @Test
        fun shouldReturnAllStores()
        {
            mvc.perform().andExpectOkAndContainsInAnyOrder(store1, store2)
            verify(exactly = 1) { storeShowService.listStores() }
        }
    }

    @Nested
    inner class GetStoreTest
    {
        @BeforeEach
        fun setup()
        {
            val captor = slot<Int>()
            every { storeFindService.find(capture(captor)) } answers {
                when (captor.captured)
                {
                    1    -> store1
                    2    -> store2
                    else -> throw ItemNotFoundException(Store::class, captor.captured)
                }
            }
        }

        private fun MockMvc.perform(id: Int): ResultActions = perform(get("/stores/$id"))

        private fun ResultActions.andExpectOkAndStore(store: Store) =
                andExpect(status().isOk).andExpect(content().contentType("application/json")).andExpectItIs(store)

        private fun ResultActions.andExpectItIs(store: Store)
        {
            this.andExpect(jsonPath("$", isA<Any>(LinkedHashMap::class.java)))
                    .andExpect(jsonPath("$.id", `is`(store.id)))
                    .andExpect(jsonPath("$.name", `is`(store.name)))
                    .andExpect(jsonPath("$.description", `is`(store.description)))
        }

        @Test
        fun shouldReturnOneStoreWhenLookingForExistingId()
        {
            mvc.perform(1).andExpectOkAndStore(store1)
            mvc.perform(2).andExpectOkAndStore(store2)

            verify(exactly = 2) { storeFindService.find(any()) }
        }

        @Test
        fun shouldReturnNotFoundWhenLookingForNonExistingId()
        {
            mvc.perform(5)
                    .andExpect(status().isNotFound)
                    .andExpect(content().contentType("text/plain;charset=UTF-8"))
                    .andExpect(content().string("Item Store with id <5> not found"))

            verify(exactly = 1) { storeFindService.find(any()) }
        }
    }

    @Nested
    inner class SaveStoreTest
    {
        private val objectMapper = ObjectMapper()

        private fun Store.toJson() = objectMapper.writeValueAsString(this)
        private fun MockMvc.perform(store: Store): ResultActions =
                perform(post("/stores").contentType("application/json").content(store.toJson()))

        @BeforeEach
        fun setup()
        {
            every { storeUpsertService.upsert(any()) } returns(Unit)
        }

        @Test
        fun shouldReturnOkWhenSavingStore()
        {
            val store = Store(3, "Store 3", "Cupboard behind the stairs")
            mvc.perform(store)
                    .andExpect(status().isOk)
                    .andExpect(content().contentType("text/plain;charset=UTF-8"))
                    .andExpect(content().string("OK"))
        }
    }
}