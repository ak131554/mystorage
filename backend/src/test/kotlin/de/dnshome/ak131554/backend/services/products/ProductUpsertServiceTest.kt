package de.dnshome.ak131554.backend.services.products

import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.repositories.ProductRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ProductUpsertServiceTest
{
    private lateinit var productRepository: ProductRepository
    private lateinit var productUpsertService: ProductUpsertService

    @BeforeEach
    fun setup()
    {
        productRepository = mockk {
            val captor = slot<Product>()
            every { save(capture(captor)) } answers {
                captor.captured
            }
        }
        productUpsertService = ProductUpsertService(productRepository)
    }

    @Test
    fun shouldSaveProduct()
    {
        val product = Product(1L, "Testprodukt 1", setOf("test1", "test2"))

        productUpsertService.upsert(product)

        verify(exactly = 1) { productRepository.save(eq(product)) }
    }
}