package de.dnshome.ak131554.backend.services.labels

import de.dnshome.ak131554.backend.models.Label
import de.dnshome.ak131554.backend.repositories.LabelRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class LabelShowServiceTest
{
    private val label1 = Label("Label 1", setOf("test 1", "test 2"))
    private val label2 = Label("Label 2", setOf("test 2", "test 3"))

    private lateinit var labelRepository: LabelRepository
    private lateinit var labelShowService: LabelShowService

    @BeforeEach
    fun setup()
    {
        labelRepository = mockk {
            every { findAll() } returns(listOf(label1, label2))
        }
        labelShowService = LabelShowService(labelRepository)
    }

    @Test
    fun shouldListAllLabels()
    {
        assertThat(labelShowService.listLabels(), containsInAnyOrder(label1, label2))
        verify(exactly = 1) { labelRepository.findAll() }
    }
}