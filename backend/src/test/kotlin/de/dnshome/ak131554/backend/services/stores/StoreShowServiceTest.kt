package de.dnshome.ak131554.backend.services.stores

import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.repositories.StoreRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class StoreShowServiceTest
{
    private val store1 = Store(1, "Store 1", "In Basement")
    private val store2 = Store(2, "Store 2", "In Attic")

    private lateinit var storeRepository: StoreRepository
    private lateinit var storeShowService: StoreShowService

    @BeforeEach
    fun setup()
    {
        storeRepository = mockk {
            every { findAll() } returns(listOf(store1, store2))
        }
        storeShowService = StoreShowService(storeRepository)
    }

    @Test
    fun shouldListAllStores()
    {
        assertThat(storeShowService.listStores(), containsInAnyOrder(store1, store2))
        verify(exactly = 1) { storeRepository.findAll() }
    }
}