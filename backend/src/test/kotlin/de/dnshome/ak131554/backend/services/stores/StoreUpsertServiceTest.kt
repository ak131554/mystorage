package de.dnshome.ak131554.backend.services.stores

import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.repositories.StoreRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class StoreUpsertServiceTest
{
    private lateinit var storeRepository: StoreRepository
    private lateinit var storeUpsertService: StoreUpsertService

    @BeforeEach
    fun setup()
    {
        storeRepository = mockk {
            val captor = slot<Store>()
            every { save(capture(captor)) } answers {
                captor.captured
            }
        }
        storeUpsertService = StoreUpsertService(storeRepository)
    }

    @Test
    fun shouldSaveStore()
    {
        val store = Store(1, "Store 1", "In Basement")

        storeUpsertService.upsert(store)

        verify(exactly = 1) { storeRepository.save(eq(store)) }
    }
}