package de.dnshome.ak131554.backend.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("products")
data class Product(@Id val globalTradeItemNumber: Long, val name: String, val labels: Set<String>)