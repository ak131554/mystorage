package de.dnshome.ak131554.backend.services.stores

import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.repositories.StoreRepository
import org.springframework.stereotype.Service

@Service
class StoreShowService(private val storeRepository: StoreRepository)
{
    fun listStores(): List<Store> = storeRepository.findAll()
}