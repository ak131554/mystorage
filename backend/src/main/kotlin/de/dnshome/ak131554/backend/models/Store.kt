package de.dnshome.ak131554.backend.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("stores")
data class Store(@Id val id: Int, val name: String, val description: String)