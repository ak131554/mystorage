package de.dnshome.ak131554.backend.repositories

import de.dnshome.ak131554.backend.models.Store
import org.springframework.data.mongodb.repository.MongoRepository

interface StoreRepository : MongoRepository<Store, Int>