package de.dnshome.ak131554.backend.services.stocks

import de.dnshome.ak131554.backend.models.Stock
import de.dnshome.ak131554.backend.repositories.StockRepository
import org.springframework.stereotype.Service

@Service
class StockShowService(private val stockRepository: StockRepository)
{
    fun listStocks(gtin: Long?): List<Stock> = gtin?.let { stockRepository.findAllByProductGlobalTradeItemNumber(it) } ?: stockRepository.findAll()
}