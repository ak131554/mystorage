package de.dnshome.ak131554.backend.services.stores

import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.repositories.StoreRepository
import org.springframework.stereotype.Service

@Service
class StoreUpsertService(private val storeRepository: StoreRepository)
{
    fun upsert(store: Store)
    {
        storeRepository.save(store)
    }
}