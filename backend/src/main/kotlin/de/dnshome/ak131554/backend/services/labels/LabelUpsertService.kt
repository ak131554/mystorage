package de.dnshome.ak131554.backend.services.labels

import de.dnshome.ak131554.backend.models.Label
import de.dnshome.ak131554.backend.repositories.LabelRepository
import org.springframework.stereotype.Service

@Service
class LabelUpsertService(private val labelRepository: LabelRepository)
{
    fun upsert(label: Label)
    {
        labelRepository.save(label)
    }
}