package de.dnshome.ak131554.backend.services.products

import de.dnshome.ak131554.backend.repositories.ProductRepository
import org.springframework.stereotype.Service

@Service
class ProductShowService(private val productRepository: ProductRepository)
{
    fun listProducts(label: String?) = label?.let { productRepository.findByLabelsContains(it) } ?: productRepository.findAll()
}