package de.dnshome.ak131554.backend.repositories

import de.dnshome.ak131554.backend.models.Product
import org.springframework.data.mongodb.repository.MongoRepository

interface ProductRepository : MongoRepository<Product, Long>
{
    fun findByLabelsContains(label: String): List<Product>
}