package de.dnshome.ak131554.backend.controllers

import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.services.stores.StoreFindService
import de.dnshome.ak131554.backend.services.stores.StoreShowService
import de.dnshome.ak131554.backend.services.stores.StoreUpsertService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/stores")
@CrossOrigin
class StoreController(private val storeFindService: StoreFindService,
                      private val storeShowService: StoreShowService,
                      private val storeUpsertService: StoreUpsertService)
{
    @GetMapping
    fun listStores() = storeShowService.listStores()

    @GetMapping("/{id}")
    fun getStore(@PathVariable id: Int) = storeFindService.find(id)

    @PostMapping
    fun saveStore(@RequestBody store: Store): String
    {
        storeUpsertService.upsert(store)
        return "OK"
    }
}