package de.dnshome.ak131554.backend.controllers

import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.services.products.ProductFindService
import de.dnshome.ak131554.backend.services.products.ProductShowService
import de.dnshome.ak131554.backend.services.products.ProductUpsertService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/products")
@CrossOrigin
class ProductController(private val productFindService: ProductFindService,
                        private val productShowService: ProductShowService,
                        private val productUpsertService: ProductUpsertService)
{
    @GetMapping
    fun listProducts(@RequestParam(required = false) label: String?) = productShowService.listProducts(label)

    @GetMapping("/{id}")
    fun getProduct(@PathVariable id: Long) = productFindService.find(id)

    @PostMapping
    fun saveProduct(@RequestBody product: Product): String
    {
        productUpsertService.upsert(product)
        return "OK"
    }
}