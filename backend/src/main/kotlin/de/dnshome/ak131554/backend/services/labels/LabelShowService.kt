package de.dnshome.ak131554.backend.services.labels

import de.dnshome.ak131554.backend.models.Label
import de.dnshome.ak131554.backend.repositories.LabelRepository
import org.springframework.stereotype.Service

@Service
class LabelShowService(private val labelRepository: LabelRepository)
{
    fun listLabels(): List<Label> = labelRepository.findAll()
}