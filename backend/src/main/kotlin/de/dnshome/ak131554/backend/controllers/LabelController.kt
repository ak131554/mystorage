package de.dnshome.ak131554.backend.controllers

import de.dnshome.ak131554.backend.models.Label
import de.dnshome.ak131554.backend.services.labels.LabelFindService
import de.dnshome.ak131554.backend.services.labels.LabelShowService
import de.dnshome.ak131554.backend.services.labels.LabelUpsertService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/labels")
@CrossOrigin
class LabelController(private val labelFindService: LabelFindService,
                      private val labelShowService: LabelShowService,
                      private val labelUpsertService: LabelUpsertService)
{
    @GetMapping
    fun listLabels() = labelShowService.listLabels()

    @GetMapping("/{name}")
    fun getLabel(@PathVariable name: String) = labelFindService.find(name)

    @PostMapping
    fun saveLabel(@RequestBody label: Label): String
    {
        labelUpsertService.upsert(label)
        return "OK"
    }
}