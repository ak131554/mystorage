package de.dnshome.ak131554.backend.services.stocks

import de.dnshome.ak131554.backend.models.Stock
import de.dnshome.ak131554.backend.repositories.StockRepository
import org.springframework.stereotype.Service

@Service
class StockUpsertService(private val stockRepository: StockRepository)
{
    fun upsert(stock: Stock)
    {
        stockRepository.save(stock)
    }
}