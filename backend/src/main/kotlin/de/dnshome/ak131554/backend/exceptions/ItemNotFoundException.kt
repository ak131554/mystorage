package de.dnshome.ak131554.backend.exceptions

import kotlin.reflect.KClass

class ItemNotFoundException(itemClass: KClass<out Any>, id: Any) : Exception("Item ${itemClass.simpleName} with id <$id> not found")