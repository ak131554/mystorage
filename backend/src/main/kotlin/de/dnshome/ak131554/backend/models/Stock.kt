package de.dnshome.ak131554.backend.models

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Unwrapped
import java.time.LocalDate
import java.util.UUID

@Document("products2Stores")
data class Stock(@Id val id: UUID,
                 @Unwrapped.Nullable(prefix = "product_") val product: Product,
                 @Unwrapped.Nullable(prefix = "store_") val store: Store,
                 val amount: Int,
                 @JsonFormat(pattern="yyyy-MM-dd") val bestBeforeDate: LocalDate)