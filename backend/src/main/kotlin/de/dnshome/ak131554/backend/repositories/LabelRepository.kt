package de.dnshome.ak131554.backend.repositories

import de.dnshome.ak131554.backend.models.Label
import org.springframework.data.mongodb.repository.MongoRepository

interface LabelRepository : MongoRepository<Label, String>
{
    fun findByName(name: String) = findById(name)
}