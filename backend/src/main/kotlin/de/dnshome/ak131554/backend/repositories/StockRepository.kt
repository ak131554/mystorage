package de.dnshome.ak131554.backend.repositories

import de.dnshome.ak131554.backend.models.Stock
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.UUID

interface StockRepository : MongoRepository<Stock, UUID>
{
    fun findAllByProductGlobalTradeItemNumber(gtin: Long): List<Stock>
}