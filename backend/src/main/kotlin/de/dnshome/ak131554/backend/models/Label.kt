package de.dnshome.ak131554.backend.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("labels")
data class Label(@Id val name: String, val labels: Set<String>)