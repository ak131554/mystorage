package de.dnshome.ak131554.backend.services.products

import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.repositories.ProductRepository
import org.springframework.stereotype.Service

@Service
class ProductUpsertService(private val productRepository: ProductRepository)
{
    fun upsert(product: Product)
    {
        productRepository.save(product)
    }
}