package de.dnshome.ak131554.backend.controllers

import de.dnshome.ak131554.backend.models.Stock
import de.dnshome.ak131554.backend.services.stocks.StockShowService
import de.dnshome.ak131554.backend.services.stocks.StockUpsertService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/stocks")
@CrossOrigin
class StockController(private val stockShowService: StockShowService,
                      private val stockUpsertService: StockUpsertService)
{
    @GetMapping
    fun listStocks(@RequestParam(required = false) gtin: Long?) = stockShowService.listStocks(gtin)

    @PostMapping
    fun saveStock(@RequestBody stock: Stock): String
    {
        println(stock.id)
        stockUpsertService.upsert(stock)
        return "OK"
    }
}