package de.dnshome.ak131554.backend.services.stores

import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Store
import de.dnshome.ak131554.backend.repositories.StoreRepository
import org.springframework.stereotype.Service

@Service
class StoreFindService(private val storeRepository: StoreRepository)
{
    fun find(id: Int): Store = storeRepository.findById(id).orElseThrow { ItemNotFoundException(Store::class, id) }
}