package de.dnshome.ak131554.backend.services.labels

import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Label
import de.dnshome.ak131554.backend.repositories.LabelRepository
import org.springframework.stereotype.Service

@Service
class LabelFindService(private val labelRepository: LabelRepository)
{
    fun find(name: String): Label = labelRepository.findByName(name).orElseThrow { ItemNotFoundException(Label::class, name) }
}