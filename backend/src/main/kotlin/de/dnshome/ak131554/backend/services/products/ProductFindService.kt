package de.dnshome.ak131554.backend.services.products

import de.dnshome.ak131554.backend.exceptions.ItemNotFoundException
import de.dnshome.ak131554.backend.models.Product
import de.dnshome.ak131554.backend.repositories.ProductRepository
import org.springframework.stereotype.Service

@Service
class ProductFindService(private val productRepository: ProductRepository)
{
    fun find(id: Long): Product = productRepository.findById(id).orElseThrow { ItemNotFoundException(Product::class, id) }
}