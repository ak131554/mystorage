# MyStorage

## Description
MyStorage is a library of microservices that should work together to form an application for stockkeeping food and beverages in a household. It originates from requirement to store preserved products like food in cans and jars, rice, noodles, mineral water and so on as a preparation for disasters and catastrophic situations.

## Goals
* Overview what products are in store and where they are stored.
* Overview about the best-before dates of the product.
* Easy and intuitive user interface to check newly bought products in and consumed products out.
* Automatic notification when a product's best-before date is nearly reached 

## Overview

### backend
This microservice defines the Backend with all its fields and is responsible for storing and 
retrieving Products in and from a persistence layer.

## License
Copyright (C) 2021-2022 Andreas Kranz

This library is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

## Project status
Early phase

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)