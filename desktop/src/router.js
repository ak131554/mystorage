import { createWebHistory, createRouter } from 'vue-router'

import HomeView from './components/Home.vue'
import AboutView from './components/About.vue'
import NotFoundView from './components/NotFound.vue'

const routes = [
  { path: '/', component: HomeView, alias: '/home' },
  { path: '/about/:label', component: AboutView },
  { path: '/:notFound(.*)+', component: NotFoundView },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router