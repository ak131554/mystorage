const {protocol, host} = window.location;
const {VITE_API_ENDPOINT} = import.meta.env;

const ENDPOINTS = {
    /**
     * Retrieves the API Endpoint URL.
     * If VITE_API_ENDPOINT starts with 'http' then the whole url will be overwritten,
     * otherwise VITE_API_ENDPOINT will be interpreted as a subdomain.
     */
    API: VITE_API_ENDPOINT.startsWith('http')
        ? VITE_API_ENDPOINT
        : `${protocol}//${VITE_API_ENDPOINT}.${host}`,
};

export default ENDPOINTS;