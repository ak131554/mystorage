import { afterEach, beforeEach, it, expect, describe, vi } from 'vitest';

describe('config.js', () => {
  beforeEach(() => {
    vi.resetModules();
  });

  afterEach(() => {
    vi.unstubAllEnvs();
    vi.unstubAllGlobals();
  });

  it.each([
    {
      endpoint: 'http://localhost:3002',
      protocol: undefined,
      host: undefined,
      expectedResult: 'http://localhost:3002',
    },
    {
      endpoint: 'production',
      protocol: 'https:',
      host: 'dummy.org',
      expectedResult: 'https://production.dummy.org',
    },
  ])('API($endpoint) -> $expectedResult', async ({ endpoint, protocol, host, expectedResult }) => {
    vi.stubEnv('API_ENDPOINT', endpoint);
    const locationMock = {
      protocol,
      host,
    };
    vi.stubGlobal('location', locationMock);

    const module = await import('./config');

    expect(module.default.API).toBe(expectedResult);
  });
});
