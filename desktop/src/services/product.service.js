import axios from "axios";
import ENDPOINTS from "@/services/adapter/config";

const API = `${ENDPOINTS.API}/products`;

const productService = {
    async getProductsByLabel(label) {
        return axios.get(`${API}?label=${label}`)
    }
};

export default productService;