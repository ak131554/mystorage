#file: noinspection SpellCheckingInspection
# Template Kaniko
.kaniko:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [ "" ]
  script:
    - 'echo "{\"builded_at\": \"$(date -u -Iminutes)\", \"git_branch\": \"${CI_COMMIT_REF_NAME}\", \"git_sha\": \"${CI_COMMIT_SHA}\"}" > $SUB_PROJECT/build.json'
    - if [[ ${KANIKO_BUILD_TARGET} ]]; then echo ${KANIKO_BUILD_TARGET}; /kaniko/executor --context "${SUB_PROJECT}" --skip-unused-stages --target ${KANIKO_BUILD_TARGET} --dockerfile "${SUB_PROJECT}/Dockerfile" --destination "${IMAGE_TAG}"; else echo "No KANIKO_BUILD_TARGET"; /kaniko/executor --context "${SUB_PROJECT}" --dockerfile "${SUB_PROJECT}/Dockerfile" --destination "${IMAGE_TAG}"; fi

.kaniko_build:
  extends: .kaniko
  stage: build
  before_script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json

# Template Container-Scanning
.container_scan:
  stage: scan
  image:
    name: aquasec/trivy
    entrypoint: [ "" ]
  variables:
    TRIVY_AUTH_URL: $CI_REGISTRY
    TRIVY_USERNAME: $CI_REGISTRY_USER
    TRIVY_PASSWORD: $CI_REGISTRY_PASSWORD
    TRIVY_NO_PROGRESS: "true"
    TRIVY_SKIP_DIRS: "/usr/local/bundle/ruby/"
    GIT_STRATEGY: none
  script:
    - trivy --version
    # cache cleanup is needed when scanning images with the same tags, it does not remove the database
    - time trivy clean --scan-cache
    # update vulnerabilities db
    - time trivy --cache-dir .trivycache/ image --download-db-only
    # Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
    - time trivy --cache-dir .trivycache/ image --exit-code 0 --format template --template "@/contrib/gitlab.tpl" --output "$CI_PROJECT_DIR/gl-container-scanning-report.json" "$IMAGE_TAG"
    # Prints full report
    - time trivy --cache-dir .trivycache/ image --exit-code 0 "$IMAGE_TAG"
    # Fails on high and critical vulnerabilities
    - time trivy --cache-dir .trivycache/ image --vuln-type library --exit-code 1 --severity HIGH,CRITICAL "$IMAGE_TAG"
  cache:
    paths:
      - .trivycache/
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json

# ------ Gradle jobs ------
.gradle: &gradle_configuration
  image: gradle:jdk21-alpine
  before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradle

.gradle_build:
  stage: build
  <<: *gradle_configuration
  script:
    - ./gradlew --build-cache --gradle-user-home cache/ :$SUB_PROJECT:assemble
  cache:
    key:
      files:
        - gradle/wrapper/gradle-wrapper.properties
      prefix: $CI_COMMIT_REF_SLUG
    paths:
      - .gradle
      - cache/caches
      - cache/notifications
      - cache/wrapper
  artifacts:
    paths:
      - $SUB_PROJECT/build
    expire_in: 1 week

.gradle_test:
  stage: test
  <<: *gradle_configuration
  script:
    - ./gradlew --build-cache --gradle-user-home cache/ :$SUB_PROJECT:test
  cache:
    key:
      files:
        - gradle/wrapper/gradle-wrapper.properties
      prefix: $CI_COMMIT_REF_SLUG
    paths:
      - .gradle
      - cache/caches
      - cache/notifications
      - cache/wrapper
  artifacts:
    paths:
      - $SUB_PROJECT/build/jacoco/jacoco.xml
    reports:
      junit:
        - $SUB_PROJECT/build/test-results/test/TEST-*.xml

.gradle_coverage:
  stage: coverage
  image: registry.gitlab.com/haynes/jacoco2cobertura:1.0.7
  script:
    - python /opt/cover2cover.py $CI_PROJECT_DIR/$SUB_PROJECT/build/jacoco/jacoco.xml $CI_PROJECT_DIR/$SUB_PROJECT/src/main/java/ > $CI_PROJECT_DIR/$SUB_PROJECT/build/cobertura.xml
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: $SUB_PROJECT/build/cobertura.xml

.gradle_sbom:
  stage: scan
  <<: *gradle_configuration
  #only:
  #  refs:
  #    - master
  #    - /^R-.*/
  script:
    - ./gradlew --build-cache --gradle-user-home cache/ :$SUB_PROJECT:cyclonedxBom
  cache:
    key:
      files:
        - gradle/wrapper/gradle-wrapper.properties
      prefix: $CI_COMMIT_REF_SLUG
    paths:
      - .gradle
      - cache/caches
      - cache/notifications
      - cache/wrapper
  artifacts:
    paths:
      - $SUB_PROJECT/bom.json
    expire_in: never

.gradle_kaniko_build:
  extends: .kaniko_build
  stage: image
  variables:
    KANIKO_BUILD_TARGET: ci-stage